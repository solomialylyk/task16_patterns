package solomia.factoryMethod;

public enum  Model {
    Cheese, Veggie, Pepperoni, Clam
}
