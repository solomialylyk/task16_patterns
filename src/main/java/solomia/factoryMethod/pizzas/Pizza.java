package solomia.factoryMethod.pizzas;

public interface Pizza {
    String sauce = null;
    String topping = null;
    String sausages = null;
    void order();
    void box();
    void bake();
    void call(String sauce, String topping, String sausages);
}
