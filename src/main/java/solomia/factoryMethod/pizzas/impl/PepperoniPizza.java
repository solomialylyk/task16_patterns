package solomia.factoryMethod.pizzas.impl;

import solomia.factoryMethod.pizzas.Pizza;

public class PepperoniPizza implements Pizza {
    private String pepperoni="Pepperoni";
    public void call(String sauce, String topping, String sausages) {
        System.out.println("Pizza with: " + "Sause " + sauce + ", Topping " + topping + ", "+ pepperoni+ ", Sausages " + sausages+ " are done!");
    }

    public void bake() {
        System.out.println("Baking");
    }

    @Override
    public void order() {
        System.out.println("Order received");
    }

    public void box() {
        System.out.println("Boxing");
    }


}
