package solomia.factoryMethod.pizzas.impl;

import solomia.factoryMethod.pizzas.Pizza;

public class CheesePizza implements Pizza {
    private String cheese = "Mozzarella";

    public void call(String sauce, String topping, String sausages) {
        System.out.println("Pizza with: " + " Cheese " + cheese + ", Sause " + sauce + ", Topping " + topping + ", Sausages " + sausages + " are done!");
    }

    public void bake() {
        System.out.println("Baking");
    }

    @Override
    public void order() {
        System.out.println("Order received");
    }

    public void box() {
        System.out.println("Boxing");
    }


}
