package solomia.factoryMethod.pizzas.impl;

import solomia.factoryMethod.pizzas.Pizza;

public class VeggiePizza implements Pizza {
    @Override
    public void call(String sauce, String topping, String sausages) {
        System.out.println("Pizza with: " + "Sause " + sauce + ", Topping " + topping + ", Sausages " + sausages+ " are done!");
    }
    @Override
    public void bake() {
        System.out.println("Baking");
    }

    @Override
    public void order() {
        System.out.println("Order received");
    }
    @Override
    public void box() {
        System.out.println("Boxing");
    }


}
