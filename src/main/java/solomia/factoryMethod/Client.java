package solomia.factoryMethod;

import solomia.factoryMethod.factory.Pizzeria;
import solomia.factoryMethod.factory.impl.KyivPizzeria;
import solomia.factoryMethod.factory.impl.LvivPizzeria;
import solomia.factoryMethod.pizzas.Pizza;
import solomia.factoryMethod.pizzas.impl.CheesePizza;

public class Client {
    public static void main(String[] args) {
        Pizzeria pizzeria = new LvivPizzeria();
        Pizza pizza = pizzeria.assemble(Model.Cheese);
        pizza.call("Tartar", null, "Bavarski");
        System.out.println();
//----------------------
        Pizza pizza1 = pizzeria.assemble(Model.Pepperoni);
        pizza1.call("Tomato", null, "Bavarski");
        System.out.println();
//----------------------
        Pizzeria pizzeria1 = new KyivPizzeria();
        Pizza pizza2 = pizzeria1.assemble(Model.Cheese);
        pizza2.call("Tartar", null, "Bavarski");

    }
}
