package solomia.factoryMethod.factory.impl;

import solomia.factoryMethod.Model;
import solomia.factoryMethod.factory.Pizzeria;
import solomia.factoryMethod.pizzas.Pizza;
import solomia.factoryMethod.pizzas.impl.CheesePizza;
import solomia.factoryMethod.pizzas.impl.PepperoniPizza;
import solomia.factoryMethod.pizzas.impl.VeggiePizza;

public class LvivPizzeria extends Pizzeria {
    @Override
    protected Pizza createPizza(Model model) {
        Pizza pizza = null;
        if (model == Model.Cheese) {
            pizza = new CheesePizza();
        } else if (model == Model.Veggie) {
            pizza = new VeggiePizza();
        } else if (model == Model.Clam) {
            pizza = new CheesePizza();
        } else if (model == Model.Pepperoni) {
            pizza = new PepperoniPizza();
        }
        return pizza;
    }
}
