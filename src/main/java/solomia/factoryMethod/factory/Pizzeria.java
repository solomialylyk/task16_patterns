package solomia.factoryMethod.factory;

import solomia.factoryMethod.Model;
import solomia.factoryMethod.pizzas.Pizza;

public abstract class Pizzeria {
    public static int count=1;
    protected abstract Pizza createPizza(Model model);

    public Pizza assemble(Model model) {
        System.out.println("Order number "+ count);
        count++;
        Pizza pizza = createPizza(model);
        pizza.order();
        pizza.bake();
        pizza.box();
        return pizza;
    }
}
